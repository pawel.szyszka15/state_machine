#include "main.h"
#include "button.h"

// static variable
static uint16_t counter;

// Button Init
void ButtonInitKey(TButton* Key, GPIO_TypeDef* GpioPort, uint16_t GpioPin, uint32_t	TimerDebounce,
					uint32_t TimerLongPress, uint32_t TimerRepeat, uint32_t TimerRelease) {
	Key->State = IDLE;

	Key->GpioPort = GpioPort;
	Key->GpioPin = GpioPin;

	Key->TimerDebounce = TimerDebounce;
	Key->TimerLongPress = TimerLongPress;
	Key->TimerRepeat = TimerRepeat;
	Key->TimerRelease = TimerRelease;
}
// Time setting functions
void ButtonSetDebounceTime(TButton* Key, uint32_t Milliseconds) {
	Key->TimerDebounce = Milliseconds;
}

void ButtonSetLongPressTime(TButton* Key, uint32_t Milliseconds) {
	Key->TimerLongPress = Milliseconds;
}

void ButtonSetRepeatTime(TButton* Key, uint32_t Milliseconds) {
	Key->TimerRepeat = Milliseconds;
}

void ButtonSetReleaseTime(TButton* Key, uint32_t Milliseconds) {
	Key->TimerRelease = Milliseconds;
}
// Register callbacks
void ButtonRegisterPressCallback(TButton* Key, void *Callback) {
	Key->ButtonPressed = Callback;
}

void ButtonRegisterLongPressCallback(TButton* Key, void *Callback) {
	Key->ButtonLongPressed = Callback;
}

void ButtonRegisterRepeatCallback(TButton* Key, void *Callback) {
	Key->ButtonRepeat = Callback;
}

void ButtonRegisterReleaseCallback(TButton* Key, void *Callback) {
	Key->ButtonRelease = Callback;
}
// States routine
void ButtonIdleRoutine(TButton* Key) {
	if(HAL_GPIO_ReadPin(Key->GpioPort,Key->GpioPin)) {
		Key->State = DEBOUNCE;
		Key->LastTick = HAL_GetTick();
	}
}

void ButtonDebounceRoutine(TButton* Key) {
	if((HAL_GetTick() - Key->LastTick) > Key->TimerDebounce) {
		if(HAL_GPIO_ReadPin(Key->GpioPort,Key->GpioPin)) {
			Key->State = PRESSED;
			Key->LastTick = HAL_GetTick();
			if(Key->ButtonPressed != NULL) {
				Key->ButtonPressed();
			}
		}
		else {
			Key->State = IDLE;
		}
	}
}

void ButtonPressedRoutine(TButton* Key) {
	if(HAL_GPIO_ReadPin(Key->GpioPort,Key->GpioPin) == GPIO_PIN_RESET) {
		Key->State = RELEASED;
		Key->LastTick = HAL_GetTick();
	} else {
		if((HAL_GetTick() - Key->LastTick) > Key->TimerLongPress) {
			Key->State = REPEAT;
			Key->LastTick = HAL_GetTick();

			if(Key->ButtonLongPressed != NULL) {
				Key->ButtonLongPressed();
			}

		}
	}
}

void ButtonRepeatRoutine(TButton* Key) {
	if(HAL_GPIO_ReadPin(Key->GpioPort,Key->GpioPin) == GPIO_PIN_RESET) {
		Key->State = RELEASED;
		Key->LastTick = HAL_GetTick();
	} else {
		if((HAL_GetTick() - Key->LastTick) > Key->TimerRepeat) {
			Key->LastTick = HAL_GetTick();  // reload timer - przeladowanie timera
			if(Key->ButtonRepeat != NULL) {
				Key->ButtonRepeat();
			}
		}
	}
}

void ButtonReleasedRoutine(TButton* Key) {

	if((HAL_GetTick() - Key->LastTick) > Key->TimerRelease) {
		Key->LastTick = HAL_GetTick();
		if(Key->ButtonRelease != NULL) {
				Key->ButtonRelease();
			}
		counter++;
	}
	if(counter == 6) {
		Key->State = IDLE;
		counter = 0;
	}

}

// State Machine
void ButtonTask(TButton* Key){
  switch(Key->State) {
    case IDLE:
        // do IDLE
    	ButtonIdleRoutine(Key);
    	break;
    case DEBOUNCE:
    	// do DEBOUNCE
    	ButtonDebounceRoutine(Key);
    	break;
    case PRESSED:
        // do PRESSED
    	ButtonPressedRoutine(Key);
    	break;
    case REPEAT:
        // do REPEAT
       	ButtonRepeatRoutine(Key);
        break;
    case RELEASED:
        // do RELEASED
        ButtonReleasedRoutine(Key);
        break;
  }
}
